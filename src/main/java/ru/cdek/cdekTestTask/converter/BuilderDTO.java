package ru.cdek.cdekTestTask.converter;

import ru.cdek.cdekTestTask.dto.CallCenterTaskDTO;
import ru.cdek.cdekTestTask.entity.CallCenterTask;

public class BuilderDTO {
    public static CallCenterTaskDTO orderToOrderDTO(CallCenterTask callCenterTask) {
        return new CallCenterTaskDTO(callCenterTask.getOrderNumber(), callCenterTask.getOrderData(), callCenterTask.getReady());
    }
}
