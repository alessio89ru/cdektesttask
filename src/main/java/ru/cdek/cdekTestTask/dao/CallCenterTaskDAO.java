package ru.cdek.cdekTestTask.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.cdek.cdekTestTask.entity.CallCenterTask;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface CallCenterTaskDAO extends CrudRepository<CallCenterTask, Long> {

    public List<CallCenterTask> findByOrderNumber(Long orderNumber);

    @Query(value = "SELECT t FROM CallCenterTask t WHERE t.ready = false AND (:orderNumber IS NULL OR t.orderNumber =:orderNumber) AND t.orderData BETWEEN :orderDataAfter AND :orderDataBefore")
    public List<CallCenterTask> getCallCenterTask(@Param("orderDataAfter") LocalDateTime orderDataAfter, @Param("orderDataBefore") LocalDateTime orderDataBefore, @Param("orderNumber") Long orderNumber);
}
