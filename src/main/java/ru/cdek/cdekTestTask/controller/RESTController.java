package ru.cdek.cdekTestTask.controller;

import org.springframework.web.bind.annotation.*;
import ru.cdek.cdekTestTask.dto.CallCenterTaskDTO;
import ru.cdek.cdekTestTask.service.CallCenterTaskService;

import java.time.LocalDateTime;
import java.util.Collection;

/**
 * Класс REST api Controller
 * Контроллер позволяет:
 * - курьеру оставить заявку по номеру заказа, что не успевает доставить груз
 * - оператору call-центра получить все необработанные заявки по диапозону дат, с возможнотью уточнения по номеру заказа
 * - оператору call-центра указать признак, что заявка обработана
 *
 * @autor Шнейдер Алексей
 */

@RestController
public class RESTController {

    private CallCenterTaskService callCenterTaskService;

    public RESTController(CallCenterTaskService callCenterTaskService) {
        this.callCenterTaskService = callCenterTaskService;
    }

    /**
     * Функция приветствия пользователя на домашней странице сервиса
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String homepage() {
        return "Сервис call-центра";
    }

    /**
     * Запрос POST
     * Добавляет заявку от курьера для обработки call-центром
     *
     * @param orderNumber - номер заказа
     */
    @RequestMapping(value = "/api/callTask/nextTime", method = RequestMethod.POST)
    @ResponseBody
    public CallCenterTaskDTO nextTime(@RequestParam("orderNumber") Long orderNumber) {
        return callCenterTaskService.save(orderNumber);
    }

    /**
     * Запрос GET
     * Выполняет поиск необработанных заявок операторами call-центра
     *
     * @param orderDataAfter  - дата от
     * @param orderDataBefore - дата до
     * @param orderNumber     - номер заказа, поле не обязательное
     */
    @RequestMapping(value = "/api/callTask/getCallCenterTask", method = RequestMethod.GET)
    public Collection<CallCenterTaskDTO> getCallCenterTask(
            @RequestParam("orderDataAfter") String orderDataAfter,
            @RequestParam("orderDataBefore") String orderDataBefore,
            @RequestParam(name = "orderNumber", required = false) Long orderNumber) {

        Collection<CallCenterTaskDTO> list = callCenterTaskService.findByOrderDataBetween(LocalDateTime.parse(orderDataAfter), LocalDateTime.parse(orderDataBefore), orderNumber);
        return list;
    }

    /**
     * Запрос PUT
     * Устанавливает признак, что заявка обработана call-центром
     *
     * @param orderNumber - номер заказа
     */
    @RequestMapping(value = "/api/callTask/isReady", method = RequestMethod.PUT)
    @ResponseBody
    public CallCenterTaskDTO isReady(@RequestParam("orderNumber") Long orderNumber){
        return callCenterTaskService.isReady(orderNumber);
    }
}
