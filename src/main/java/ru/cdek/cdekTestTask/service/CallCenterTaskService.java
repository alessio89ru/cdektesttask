package ru.cdek.cdekTestTask.service;

import org.springframework.stereotype.Service;
import ru.cdek.cdekTestTask.converter.BuilderDTO;
import ru.cdek.cdekTestTask.dao.CallCenterTaskDAO;
import ru.cdek.cdekTestTask.dto.CallCenterTaskDTO;
import ru.cdek.cdekTestTask.entity.CallCenterTask;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Класс CallCenterTaskService - обеспечивает взаимодействие контроллера и DAO.
 *
 * @autor Шнейдер Алексей
 */

@Service
public class CallCenterTaskService {

    private CallCenterTaskDAO callCenterTaskDAO;

    public CallCenterTaskService(CallCenterTaskDAO callCenterTaskDAO) {
        this.callCenterTaskDAO = callCenterTaskDAO;
    }

    private List<CallCenterTask> findByOrderNumber(Long orderNumber) {
        return callCenterTaskDAO.findByOrderNumber(orderNumber);
    }

    public CallCenterTaskDTO save(Long orderNumber) {
        List<CallCenterTask> callCenterTaskList = findByOrderNumber(orderNumber);
        if (!callCenterTaskList.isEmpty()) return BuilderDTO.orderToOrderDTO(callCenterTaskList.get(0));

        CallCenterTask callCenterTask = new CallCenterTask(orderNumber, LocalDateTime.now(), false);
        return BuilderDTO.orderToOrderDTO(callCenterTaskDAO.save(callCenterTask));

    }

    public CallCenterTaskDTO isReady(Long orderNumber) {
        List<CallCenterTask> callCenterTaskList = findByOrderNumber(orderNumber);
        if (callCenterTaskList.isEmpty()) {
            throw new RuntimeException("Заявка не найдена по номеру заказа : " + orderNumber.toString());
        }

        CallCenterTask callCenterTask = callCenterTaskList.get(0);
        callCenterTask.setReady(Boolean.TRUE);
        return BuilderDTO.orderToOrderDTO(callCenterTaskDAO.save(callCenterTask));

    }

    public List<CallCenterTaskDTO> findByOrderDataBetween(LocalDateTime orderDataAfter, LocalDateTime orderDataBefore, Long orderNumber) {

        List<CallCenterTask> callCenterTaskList = callCenterTaskDAO.getCallCenterTask(orderDataAfter, orderDataBefore, orderNumber);

        List<CallCenterTaskDTO> callCenterTaskDTOList = new ArrayList<>();

        for (CallCenterTask callCenterTask : callCenterTaskList) {
            callCenterTaskDTOList.add(BuilderDTO.orderToOrderDTO(callCenterTask));
        }
        return callCenterTaskDTOList;
    }

}
