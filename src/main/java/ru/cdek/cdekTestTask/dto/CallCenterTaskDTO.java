package ru.cdek.cdekTestTask.dto;

import java.time.LocalDateTime;

public class CallCenterTaskDTO {
    private Long orderNumber;
    private LocalDateTime orderData;
    private Boolean ready;

    public CallCenterTaskDTO(Long orderNumber, LocalDateTime orderData, Boolean ready) {
        this.orderNumber = orderNumber;
        this.orderData = orderData;
        this.ready = ready;
    }

    public CallCenterTaskDTO() {
    }

    public Long getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Long orderNumber) {
        this.orderNumber = orderNumber;
    }

    public LocalDateTime getOrderData() {
        return orderData;
    }

    public void setOrderData(LocalDateTime orderData) {
        this.orderData = orderData;
    }

    public Boolean getReady() {
        return ready;
    }

    public void setReady(Boolean ready) {
        this.ready = ready;
    }
}
