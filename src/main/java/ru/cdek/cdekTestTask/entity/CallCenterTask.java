package ru.cdek.cdekTestTask.entity;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "LISTCALLCENTERTASK")
public class CallCenterTask {

    @Id
    @GeneratedValue
    @Column(name = "Id", nullable = false)
    private Long Id;

    @Column(name = "Order_Number", nullable = false)
    private Long orderNumber;

    @Column(name = "Order_Data", nullable = false)
    private LocalDateTime orderData;

    @Column(name = "Is_Ready", nullable = false)
    private Boolean ready;

    public CallCenterTask(Long orderNumber, LocalDateTime orderData, Boolean ready) {
        this.orderNumber = orderNumber;
        this.orderData = orderData;
        this.ready = ready;
    }

    public CallCenterTask() {
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public Long getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Long orderNumber) {
        this.orderNumber = orderNumber;
    }

    public LocalDateTime getOrderData() {
        return orderData;
    }

    public void setOrderData(LocalDateTime orderData) {
        this.orderData = orderData;
    }

    public Boolean getReady() {
        return ready;
    }

    public void setReady(Boolean ready) {
        this.ready = ready;
    }
}
