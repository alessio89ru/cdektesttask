CREATE sequence hibernate_sequence start WITH 1 increment BY 1;
CREATE TABLE LISTCALLCENTERTASK (
        Id bigint NOT NULL auto_increment,
        Order_number bigint NOT NULL,
        Order_data TIMESTAMP NOT NULL,
        Is_ready boolean NOT NULL,
        PRIMARY KEY (id)
        );