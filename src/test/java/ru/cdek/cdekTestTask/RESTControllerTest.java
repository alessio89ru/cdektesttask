package ru.cdek.cdekTestTask;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.cdek.cdekTestTask.controller.RESTController;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.hamcrest.core.StringContains.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Класс RESTControllerTest - выполняет интеграционное тестирование RESTController
 * Тесты:
 * - controllerIsNotNull - тест, что RESTController не null
 * - homepageIsOk - тест стартовой страницы сервиса
 * - seachIsNotEmpty - тест на заранее подготовленных данных, что запрос getCallCenterTask вернет пустой результат
 * - seachIsEmpty - тест на заранее подготовленных данных, что запрос getCallCenterTask найдет запись с нужным номером заказа
 * - postIsOkAndGetIsOk - тест добавления данных POST запросом nextTime и успешный поиск запросом getCallCenterTask ранее введенных данных
 *
 * @autor Шнейдер Алексей
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource("/application-test.properties")
public class RESTControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    RESTController RESTController;

    @Test
    public void controllerIsNotNull() throws Exception {
        assertThat(RESTController).isNotNull();
    }

    @Test
    public void homepageIsOk() throws Exception {
        this.mockMvc.perform(get("/"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Сервис call-центра")));
    }

    @Test
    @Sql(value = {"/create-before.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(value = {"/create-alter.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    public void seachIsNotEmpty() throws Exception {
        this.mockMvc.perform(get("/api/callTask/getCallCenterTask?orderDataAfter=2020-03-23T00:00:00&orderDataBefore=2020-03-23T23:59:59&orderNumber=1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("")));
    }

    @Test
    @Sql(value = {"/create-before.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(value = {"/create-alter.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    public void seachIsEmpty() throws Exception {
        this.mockMvc.perform(get("/api/callTask/getCallCenterTask?orderDataAfter=2020-03-23T00:00:00&orderDataBefore=2020-03-23T23:59:59&orderNumber=54017"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("54017")));
    }

    @Test
    public void postIsOkAndGetIsOk() throws Exception {
        this.mockMvc.perform(post("/api/callTask/nextTime?orderNumber=540018"))
                .andExpect(status().isOk());

        this.mockMvc.perform(get("/api/callTask/getCallCenterTask?orderDataAfter=2020-03-23T00:00:00&orderDataBefore=2020-03-30T23:59:59&orderNumber=540018"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("540018")));
    }
}
